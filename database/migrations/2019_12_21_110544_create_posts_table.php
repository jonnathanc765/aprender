<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unSignedBigInteger('user_id');

            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->string('excerpt')->nulable();
            $table->boolean('comments_status')->default(true);
            $table->bigInteger('comment_count')->default(0);

           $table->foreign('user_id')
                 ->references('id')->on('users');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
