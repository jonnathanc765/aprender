<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'title'             => 'required|string|min:2',
            'content'           => 'required|string|min:2',
            'excerpt'           => 'nullable|string',
            'comments_status'   => 'required',
            'topics'            => 'nullable|array',
            'subtopics'         => 'nullable|array'
        ];
    }
}
