@extends('layouts.app')

@section('head')

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/basic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">

@endsection

@section('content')







    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body" id="app">

                        <h2>Editar Post</h2>

                        <post-form
                        :images="{{ json_encode($images) }}"
                        :initial_topics="{{ json_encode($topics) }}"
                        :initial_subtopics="{{ json_encode($subtopics) }}"
                        :initial_post="{{ json_encode($post) }}">
                        </post-form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')

    <!-- Latest compiled and minified JavaScript -->
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>

    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('js/post.js') }}"></script>
    <script src="{{ asset('js/blog.js') }}"></script>

    <script>



    </script>

    @endsection

@endsection
