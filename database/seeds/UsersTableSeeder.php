<?php

use Illuminate\Database\Seeder;
use App\Traits\ImageFactory;

class UsersTableSeeder extends Seeder
{
    use ImageFactory;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        App\Image::truncate();

        $user = factory(App\User::class)->create([
        	'name' => 'Jon Snow',
        	'email' => 'jon@gmail.com'
        ]);

        $this->deleteDirectory(storage_path('/app/public/uploads'));

        for ( $i = 0 ; $i < rand(5, 20) ; $i++ ) {
            $this->saveImage($user->id, 'App\User');
        }


    }
}
