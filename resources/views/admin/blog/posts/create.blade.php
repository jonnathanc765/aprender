@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Media
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    ...
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h2>Crear un nuevo post</h2>
                        <form action="{{ route('admin.blog.post.store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="text" placeholder="Título" name="title">
                            <input type="text" placeholder="Contenido" name="content">
                            <textarea name="content" id="" cols="30" rows="10" placeholder="Contenido"></textarea>
                            <textarea name="excerpt" id="" cols="30" rows="10" placeholder="Extracto (opcional)"></textarea>
                            <select name="topic" id="">
                                <option value="" disabled>Tema</option>
                                @foreach ($topics as $topic)
                                    <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                                @endforeach
                            </select>
                            <select name="subtopic" id="">
                                <option value="" disabled>Sub-Tema</option>
                                @foreach ($subtopics as $subtopic)
                                    <option value="{{ $subtopic->id }}">{{ $subtopic->name }}</option>
                                @endforeach
                            </select>
                            <button type="submit">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>

        CKEDITOR.replace( 'content' );

        function InsertHTML() {
            // Get the editor instance that you want to interact with.
            var editor = CKEDITOR.instances.content;
            var value = document.getElementById( 'htmlToInsert' ).value;

            // Check the active editing mode.
            if ( editor.mode == 'wysiwyg' )
            {
                // Insert HTML code.
                // https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_editor.html#method-insertHtml
                editor.insertHtml( value );
            }
            else
                alert( 'You must be in WYSIWYG mode!' );
        }

    </script>

@endsection
