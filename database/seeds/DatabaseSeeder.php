<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	Schema::disableForeignKeyConstraints();


        $this->call(UsersTableSeeder::class);
        $this->call(TopicsAndSubTopicsTableSeeder::class);
        $this->call(PostsTableSeeder::class);


    	Schema::enableForeignKeyConstraints();

    }
}
