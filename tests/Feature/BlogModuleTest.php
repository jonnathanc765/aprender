<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogModuleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_load_the_post()
    {
        $user = factory(User::class)->create();

        $post = factory(Post::class)->create([
            'user_id' => $user->id,
            'title' => 'Titulo del post',
            'content' => 'Descripción del post'
        ]);

        $this->get(route('blog.post.show', $post))
        ->assertStatus(200)
        ->assertSee('Titulo del post')
        ->assertSee('Descripción del post');
    }
}
