<?php

namespace Tests\Feature;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentModuleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_makes_a_comment()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();

        $post = factory(Post::class)->create([
            'user_id' => $user->id
        ]);

        $this->actingAs($user);

        $this->from(route('blog.post.show', $post))
        ->post(route('blog.comment.store', $post), [
            'user_id' => $user->id,
            'post_id' => $post->id,
            'content' => 'Este post está muy interesante XD'
        ])
        ->assertRedirect(route('blog.post.show', $post))
        ->assertSessionHas('success');

        $this->assertDatabaseHas('comments', [
            'user_id' => $user->id,
            'post_id' => $post->id,
            'content' => 'Este post está muy interesante XD'
        ]);

    }

    public function test_it_shows_comments()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();

        $post = factory(Post::class)->create([
            'user_id' => $user->id
        ])->each(function ($post) use ($user)
        {
            factory(Comment::class)->create([
                'post_id' => $post->id,
                'user_id' => $user->id,
                'content' => 'Comentario 1'
            ]);

            factory(Comment::class)->create([
                'post_id' => $post->id,
                'user_id' => $user->id,
                'content' => 'Comentario 2'
            ]);

        });

        $this->get(route('blog.post.show', $post))
        ->assertStatus(200)
        ->assertSee('Comentario 1')
        ->assertSee('Comentario 2');
    }

     public function test_it_auth_required_to_make_a_comment()
    {

        $user = factory(User::class)->create();

        $post = factory(Post::class)->create([
            'user_id' => $user->id
        ]);

        $this->from(route('blog.post.show', $post))
        ->post(route('blog.comment.store', $post), [
            'content' => 'Este post está muy interesante XD'
        ])
        ->assertRedirect(route('login'));

        $this->assertDatabaseMissing('comments', [
            'user_id' => $user->id,
            'post_id' => $post->id,
            'content' => 'Este post está muy interesante XD'
        ]);
    }
}
