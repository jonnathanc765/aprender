<?php

use App\SubTopic;
use App\Topic;
use Illuminate\Database\Seeder;

class TopicsAndSubTopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Topic::truncate();
        App\SubTopic::truncate();

        factory(Topic::class, 5)->create()->each(function ($topic)
        {
            factory(SubTopic::class, 5)->create([
                'topic_id' => $topic->id
            ]);
        });
    }
}
