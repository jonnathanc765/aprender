@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <a href="{{ route('admin.blog.post.create') }}" class="btn btn-primary">Crea uno nuevo</a>

                <ul>
                    @forelse ($posts as $post)
                        <li># {{ $loop->iteration }} - {{ $post->title }} <a href="{{ route('admin.blog.post.edit', $post) }}" class="btn btn-warning">Editar</a>
                            <ul>
                                @foreach ($post->subTopics as $subTopic)
                                    <li>{{ $subTopic->name }} </li>
                                @endforeach
                            </ul>
                        </li>
                    @empty
                    <h3>No hay posts disponibles</h3>
                    <a href="{{ route('admin.blog.post.create') }}">Crea uno nuevo</a>
                    @endforelse
                </ul>

            </div>
        </div>
    </div>
@endsection
