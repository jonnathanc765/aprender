<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::find(1);

        App\Post::truncate();
        App\Topic::truncate();
        App\Comment::truncate();
        App\SubTopic::truncate();
        DB::table('post_topic')->truncate();
        DB::table('post_sub_topic')->truncate();


        $user = User::find(1);

        factory(App\Post::class, 5)->create([
        	'user_id' => $user->id,
        ])
        ->each(function ($post) use ($user)
        {
        	factory(App\Comment::class, rand(5,20))->create([
                'post_id' => $post->id,
                'user_id' => $user->id
        	]);

            factory(App\Topic::class, rand(1,6))->create()->each(function ($topic) use ($post)
            {
                $flag = rand(0, 1);

                if ($flag == 1) {
                    $post->topics()->attach($topic->id);
                }

                factory(App\SubTopic::class, rand(1,6))->create([
                    'topic_id' => $topic->id
                ])->each(function ($subTopic) use ($post, $flag)
                {
                    if ($flag == 1) {

                        if (rand(0,1) == 1) {
                            $post->subTopics()->attach($subTopic->id);
                        }

                    }
                });
            });

        });


    }
}
