import Vue from 'vue';


window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


Vue.component('post-form', require('./post-form.vue').default);
Vue.component('media', require('./media.vue').default);
Vue.component('media-element', require('./media-element.vue').default);


const app = new Vue({
    el: '#app',
})
