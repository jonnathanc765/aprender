<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Post;
use App\SubTopic;
use App\Topic;
use App\User;

class PostModuleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_loads_correctly_list_of_post()
    {
        $user = factory(User::class)->create([
            'name' => 'John Whick'
        ]);

        $this->actingAs($user);

        factory(Post::class)->create([
            'title' => 'Nombre del post numero 1',
            'user_id' => $user->id
        ]);
        factory(Post::class)->create([
            'title' => 'Nombre del post numero 2',
            'user_id' => $user->id
        ]);
        factory(Post::class)->create([
            'title' => 'Nombre del post numero 3',
            'user_id' => $user->id
        ]);

        $this->get(route('admin.blog.post.index'))
        ->assertStatus(200)
        ->assertSee('Nombre del post numero 1')
        ->assertSee('Nombre del post numero 2')
        ->assertSee('Nombre del post numero 3');

    }
    public function test_it_show_empty_message_post_list()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create([
            'name' => 'John Whick'
        ]);

        $this->actingAs($user);

        $this->get(route('admin.blog.post.index'))
        ->assertStatus(200)
        ->assertSee('No hay posts disponibles')
        ->assertSee('Crea uno nuevo');
    }
    public function test_it_loads_edit_form()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create([
            'name' => 'John Whick'
        ]);

        $this->actingAs($user);

        $topic = factory(Topic::class)->create([
            'name' => 'Nombre del topic'
        ]);

        $subTopic = factory(SubTopic::class)->create([
            'name' => 'Nombre del subTopic'
        ]);

        $post = Post::create([
            'title' => 'Post para editar',
            'content' => 'Contenido del post',
            'excerpt' => 'Este es el extracto del post',
            'comments_status' => '0',
            'user_id' => $user->id
        ]);

        $post->subTopics()->attach($subTopic->id);
        $post->topics()->attach($topic->id);

        $this->get(route('admin.blog.post.edit', $post))
            ->assertStatus(200)
            ->assertSee('Editar Post')
            ->assertSee('Post para editar')
            ->assertSee('Contenido del post')
            ->assertSee('Este es el extracto del post')
            ->assertSee('Nombre del subTopic')
            ->assertSee('Nombre del topic');

    }
    public function test_it_updates_post()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create([
            'name' => 'John Whick'
        ]);

        $this->actingAs($user);


        factory(Topic::class, 10)->create();

        factory(SubTopic::class, 10)->create();

        $post = factory(Post::class)->create([
            'user_id' => $user->id
        ]);

        $this
        ->from(route('admin.blog.post.edit', $post))
        ->put(route('admin.blog.post.update', $post), [
            'title' => 'Nombre del post super chevere',
            'content' => 'Este es el contenido',
            'excerpt' => 'Este es el extracto del post',
            'comments_status' => '0',
            'topics' => [1, 3, 5],
            'subtopics' => [1, 2]
        ])
        ->assertRedirect(route('admin.blog.post.index'))
        ->assertSessionHas('success', 'Guardado con éxito');


        $this->assertDatabaseHas('posts', [
            'title' => 'Nombre del post super chevere',
            'content' => 'Este es el contenido',
            'excerpt' => 'Este es el extracto del post',
            'comments_status' => '0'
        ]);

        $post = Post::find(1);

        $this->assertEquals(3, $post->topics->count());
        $this->assertEquals(2, $post->subTopics->count());
        $this->assertEquals($post->user_id, $user->id);
    }
    public function test_it_deletes_a_post()
    {

        $this->withoutExceptionHandling();
        $user = factory(User::class)->create([
            'name' => 'John Whick'
        ]);

        $this->actingAs($user);


        factory(Topic::class, 10)->create();

        factory(SubTopic::class, 10)->create();

        $post = factory(Post::class)->create([
            'user_id' => $user->id
        ]);

        $this
        ->delete(route('admin.blog.post.destroy', $post))
        ->assertRedirect(route('admin.blog.post.index'))
        ->assertSessionHas('success', 'Tu post ha sido movido a la papelera');


        $this->assertSoftDeleted('posts', [
            'id' => $post->id
        ]);

    }
    public function test_it_required_tht_title_when_create_a_post()
    {
        $user = factory(User::class)->create([
            'name' => 'John Whick'
        ]);

        $this->actingAs($user);

        factory(Topic::class, 10)->create();

        factory(SubTopic::class, 10)->create();

        $this
        ->from(route('admin.blog.post.create'))
        ->post(route('admin.blog.post.store', [
            'title' => '',
            'content' => 'Este es el contenido',
            'excerpt' => 'Este es el extracto del post',
            'comments_status' => '0',
            'topics' => [1, 3, 5],
            'subtopics' => [1, 2]
        ]))
        ->assertRedirect(route('admin.blog.post.create'))
        ->assertSessionHasErrors('title', 'The field title is required');

    }
}
