<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    Route::prefix('admin')->group(function ()
    {
        Route::prefix('post')->group(function() {
            Route::get('/', 'Admin\Blog\PostController@index')->name('admin.blog.post.index');
            Route::get('/create', 'Admin\Blog\PostController@create')->name('admin.blog.post.create');
            Route::post('/store', 'Admin\Blog\PostController@store')->name('admin.blog.post.store');
            Route::get('/edit/{post}', 'Admin\Blog\PostController@edit')->name('admin.blog.post.edit');
            Route::put('/update/{post}', 'Admin\Blog\PostController@update')->name('admin.blog.post.update');
            Route::delete('/destroy/{post}', 'Admin\Blog\PostController@destroy')->name('admin.blog.post.destroy');
        });
        Route::prefix('image')->group(function ()
        {
            Route::post('/store', 'Admin\Blog\ImageController@store')->name('admin.blog.image.store');
        });
    });

});


Route::prefix('blog')->group(function ()
{
    Route::get('/{post}', 'User\PostController@show')->name('blog.post.show');

    Route::group(['middleware' => ['auth']], function () {

        Route::prefix('comment')->group(function ()
        {
            Route::post('/store/{post}', 'User\CommentController@store')->name('blog.comment.store');
        });

    });

});

