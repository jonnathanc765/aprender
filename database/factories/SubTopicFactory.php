<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SubTopic;
use Faker\Generator as Faker;

$factory->define(SubTopic::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->sentence
    ];
});
