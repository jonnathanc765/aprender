@extends('layouts.app')

@section('content')

<div class="container blog-container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    <div class="title mt-4">
                        <h2>{{ $post->title }}</h2>
                        <p class="mb-5 text-muted"><i class="fa fa-clock-o"></i>18/10/2019</p>
                    </div>

                    <div class="content">
                        {{ $post->content }}
                    </div>

                    <div class="social d-flex justify-content-end align-items-center mt-4">
                        <span>
                            compartir en:
                        </span>
                        <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        <a href="#" class="mail"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                    </div>

                </div>
            </div>

            <div class="comment-writer mt-3">
                <div class="thumbnail d-flex justify-content-start align-items-center">
                    <img src="{{ asset('img/avatar.png') }}" alt="">
                    <p>Tu comentario</p>
                </div>
                <form action="{{ route('blog.comment.store', $post) }}" method="POST">
                    {{ csrf_field() }}
                    <textarea name="content" id="" cols="30" rows="10"></textarea>
                    <button type="submit">Publicar</button>
                </form>
            </div>

            <div class="comments">
                @foreach ($comments as $comment)
                <div class="media">
                    <img class="mr-3 rounded-circle" src="{{ asset('img/avatar.png') }}" alt="Generic placeholder image">
                    <div class="media-body">
                        <div class="date d-flex justify-content-between my-2 text-muted">
                            <h5 class="mt-0">{{ $comment->user->name }}</h5>
                            <span>{{ $comment->created_at->diffForHumans() }}</span>
                        </div>
                        {{ $comment->content }}
                    </div>


                </div>
                <hr>
                @endforeach
            </div>

        </div>
        <div class="col-md-4">

            <aside>

                <div class="input-group my-3 search">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Que deseas buscar...">
                </div>

                <div class="mostRead">
                    <h6>Lo más leído</h6>

                    <div class="item">
                        <img src="{{ asset('img/Screenshot_1.png') }}" alt="">
                        <div class="text">
                            <h5>¿Selección de carrera?</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, a.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="{{ asset('img/Screenshot_1.png') }}" alt="">
                        <div class="text">
                            <h5>¿Selección de carrera?</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, a.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="{{ asset('img/Screenshot_1.png') }}" alt="">
                        <div class="text">
                            <h5>¿Selección de carrera?</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, a.</p>
                        </div>
                    </div>

                </div>

                <div class="mostRecent">
                    <h6>Lo más reciente</h6>

                    <div class="item">
                        <img src="{{ asset('img/Screenshot_1.png') }}" alt="">
                        <div class="text">
                            <h5>¿Selección de carrera?</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, a.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="{{ asset('img/Screenshot_1.png') }}" alt="">
                        <div class="text">
                            <h5>¿Selección de carrera?</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, a.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="{{ asset('img/Screenshot_1.png') }}" alt="">
                        <div class="text">
                            <h5>¿Selección de carrera?</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, a.</p>
                        </div>
                    </div>

                </div>

            </aside>


        </div>
    </div>
</div>

<div class="container-full">
    <div class="container-fluid">
        <div class="row title">
            <div class="col-md-12 d-flex justify-content-center align-items-center">
                <h3>Elije cuando vas a rendir el examen</h3>
                <div class="date-button">
                    <a href="#" class="orange-button">Diciembre 2019</a>
                    <a href="#" class="white-button">Junio 2020</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="container">

                    <div class="owl-carousel">

                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset('img/Screenshot_88.png') }}" alt="Card image cap">
                            <div class="card-body">
                                <h6><span class="badge badge-warning">TOP VENTAS</span> Card title</h6>
                                <h5>David Faudito</h5>
                                <p class="text-muted">Aprende Adobe Illustrator en poco tiempo, y con profesionales altamente calificados</p>
                                <a href="#" class=cart-button><i class="fa fa-shopping-cart" aria-hidden="true"></i> US$ 9.90</a>
                            </div>
                        </div>

                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset('img/Screenshot_88.png') }}" alt="Card image cap">
                            <div class="card-body">
                                <h6><span class="badge badge-warning">TOP VENTAS</span> Card title</h6>
                                <h5>David Faudito</h5>
                                <p class="text-muted">Aprende Adobe Illustrator en poco tiempo, y con profesionales altamente calificados</p>
                                <a href="#" class=cart-button><i class="fa fa-shopping-cart" aria-hidden="true"></i> US$ 9.90</a>
                            </div>
                        </div>

                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset('img/Screenshot_88.png') }}" alt="Card image cap">
                            <div class="card-body">
                                <h6><span class="badge badge-warning">TOP VENTAS</span> Card title</h6>
                                <h5>David Faudito</h5>
                                <p class="text-muted">Aprende Adobe Illustrator en poco tiempo, y con profesionales altamente calificados</p>
                                <a href="#" class=cart-button><i class="fa fa-shopping-cart" aria-hidden="true"></i> US$ 9.90</a>
                            </div>
                        </div>

                    </div>

                    <div class="text-center show-more">
                        <a href="#">Ver todos los cursos</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

    <script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>

    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                450:{
                    items:2
                },
                767:{
                    items:3
                }
            }
        });
    </script>


@endsection
