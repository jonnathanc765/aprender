<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'title', 'content', 'excerpt', 'comments_status'];

	public function topics()
	{
		return $this->belongsToMany('App\Topic');
    }
    public function subTopics()
	{
		return $this->belongsToMany('App\SubTopic');
    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
